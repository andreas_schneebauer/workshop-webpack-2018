const webpackMerge = require("webpack-merge");


const applyPresets = env =>{
    const {presets} = env;
    if(presets !== undefined){
    const mergedPresets=[].concat(...[presets]);
    const mergedConfigs = mergedPresets.map(
        presetName => require(`./presets/webpack.${presetName}.config.js`)(env)
    );

    return webpackMerge({}, ...mergedConfigs);
}
}

module.exports = applyPresets;