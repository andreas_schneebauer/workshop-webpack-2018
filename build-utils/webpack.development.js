module.exports= () =>({
    output:{
        filename:"dev.js"
    },

    module:{
        rules:[
            {
                test: /\.css$/,
                use:[
                    "style-loader",
                    "css-loader"
                ]
            }
        ]
    }

});