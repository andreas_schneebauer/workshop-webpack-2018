import {red, blue} from "./button-styles.js";

const top = document.createElement("div");
top.innerText="top of footer";
top.style=blue;

const bottom = document.createElement("div");
bottom.innerHTML="bottom of footer";
bottom.style=red;

const footer = document.createElement("footer");
footer.appendChild(top);
footer.appendChild(bottom);

export { top, bottom, footer };
