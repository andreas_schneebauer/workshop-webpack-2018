const makeImage= (url) =>{
    const image = document.createElement("img");
    image.src=url;
    image.width="100";
    image.height="100";
    return image;
}

export default makeImage;