import nav from "./nav";
//import * as GSAP from "gsap";
const getGSAP =()=>import("gsap");
import makeButton from "./button";
import { makeColorStyle } from "./button-styles";
import imageUrl from "./webpack-logo.jpg";
import makeImage from "./image";
//import Foo from "./foo.ts";

import css from "./footer.css";
import buttonStyles from "./button.css";

const image = makeImage(imageUrl);
const button = makeButton("My first button!");

//vorteil ist dann im webpack cache, kann oefter ausgefuehrt werden ohne reload
const getFooter =()=>import("./footer");
button.style=makeColorStyle("cyan");
document.body.appendChild(button);

button.addEventListener("click", function(){
  getFooter().then(m=>{
    document.body.appendChild(m.footer);
  });
})

document.body.appendChild(image)
console.log(
  nav(),
  makeColorStyle("cyan")
);
